/**
 * Author: GNOMJOGSON
 * Date: 23.06.2015
 * Created: 17:09
 **/
(function(window){

    Controller.prototype.constructor = Controller;
    Controller.prototype = {
        deviceType: "", //computer || tablet || phone
        pageId: "", //wordpress page ID
        themePath: "", //path to wordpress theme
        breakpoints:{ // screen resolutions breakpoints
            screen_xs: 480,
            screen_sm: 768,
            screen_md: 1030,
            screen_lg: 1240,
            screen_xlg: 1400
        }
    }

    var $, ref, $body, $shapeLeft, $shapeRight, $overlayAbout, $overlayFellowship, $faqItems;
    function Controller(jQuery){

        $ = jQuery;
        ref= this;

        Logger.useDefaults();
        //Logger.setLevel(Logger.OFF);

        console.log('hello')

    }

    Controller.prototype.init = function(){

        this.deviceType = window.deviceType;
        this.pageId = window.currentPageId;
        this.themePath = window.themePath;

        Logger.info("Startup site on deviceType: " + this.deviceType + " pageId: " + this.pageId + ", width: " + ref.viewport().width + ", height: " + ref.viewport().height + ", screensize: " + ref.viewport().screensize);

        $body = $('body');
        $shapeLeft = $('.shape-container.left');
        $shapeRight = $('.shape-container.right');
        $overlayAbout = $('.overlay.about');
        $overlayFellowship = $('.overlay.fellowship');

        //the shape interaction
        $('.hitarea.left .hitarea-inner').mouseover(function() {
            $shapeLeft.addClass('hover');
            $shapeRight.removeClass('hover');
        }).mouseout(function() {
            $shapeLeft.removeClass('hover');
        }).click(function(e){
            //open about overlay
            $overlayAbout.scrollTop(0).addClass('show');
            $body.addClass('overlay-open');
            history.replaceState(null, null, $overlayAbout.attr('data-link'));
        });

        $('.hitarea.right .hitarea-inner').mouseover(function() {
            $shapeRight.addClass('hover');
            $shapeLeft.removeClass('hover');
        }).mouseout(function() {
            $shapeRight.removeClass('hover');
        }).click(function(e){
            //open fellowship overlay
            $overlayFellowship.scrollTop(0).addClass('show');
            $body.addClass('overlay-open');
            history.replaceState(null, null, $overlayFellowship.attr('data-link'));
        });

        $('.btn-close').click(function(e){
            e.preventDefault();
            $(window).scrollTop(0);
            var $overlay = $(this).closest('.overlay');
            $overlay.removeClass('show');
            $body.removeClass('overlay-open');
            history.replaceState(null, null, window.siteURL);
        });
        $('.btn-totop').click(function(e){
            e.preventDefault();
            var $overlay = $(this).closest('.overlay');
            Logger.log($overlay.scrollTop());
            $overlay.animate({
                scrollTop: 0
            }, 200);
        });


        $faqItems = $('.faq-item');
        $('.faq-headline').click(function(e){
            e.preventDefault();
            var $item = $(this).closest('.faq-item');
            if(!$item.hasClass('open')){
                //open
                TweenLite.set([$item.find('.faq-content'),$item.find('.faq-content-inner-wrap')], {height:"auto"});
                TweenLite.from([$item.find('.faq-content'),$item.find('.faq-content-inner-wrap')], 0.2, {height:0});
                $item.addClass("open");
            } else {
                //close
                TweenLite.to([$item.find('.faq-content'),$item.find('.faq-content-inner-wrap')], 0.2, {height:0});
                $item.removeClass("open");
            }
        });

        //font size changer
        $('.web-btn').click(function() {
            $('.FUTUREPET').addClass('heal-the-web').removeClass('heal-the-world')
        });

        $('.world-btn').click(function () {
            $('.FUTUREPET').removeClass('heal-the-web').addClass('heal-the-world');
        });



        //letters animation test two
        
      





// letters animation test 4
var $elements = $('#glyph1, #glyph2, #glyph3, #glyph4,#glyph5, #glyph6, #glyph7, #glyph8, #glyph9, #glyph10, #glyph11, #glyph12, #glyph13, #glyph14, #glyph15, #glyph16, #glyph17, #glyph18, #glyph19, #glyph20, #glyph21, #glyph22, #glyph23, #glyph24');

function anim_loop(index) {
    $elements.eq(index).fadeIn(50, function() {
        var $self = $(this);
        setTimeout(function() {
            $self.fadeOut(0);
            anim_loop((index + 1) % $elements.length);
        }, 1000);
    });
}

anim_loop(0); // start with the first element
    
            
        

        //change sets

        
    
        
        $('.ALT1').click(function () {
            $('.FUTUREPET').css({textTransform:'none'});
            $('.FUTUREPET').removeClass('ss02 ss03 ss04 ss05 ss06').addClass('ss01');

        });


        $('.ALT2').click(function () {
            $('.FUTUREPET').css({textTransform:'none'});
            $('.FUTUREPET').removeClass('ss01 ss03 ss04 ss05 ss06').addClass('ss02');
           
        });
        
        $('.ALT3').click(function () {
            $('.FUTUREPET').css({textTransform:'none'}); 
            $('.FUTUREPET').removeClass('ss01 ss02 ss04 ss05 ss06').addClass('ss03');
                
            });
        
        $('.ALT4').click(function () {
            $('.FUTUREPET').css({textTransform:'none'});    
            $('.FUTUREPET').removeClass('ss01 ss02 ss03 ss05 ss06').addClass('ss04');
               
            });
        
         $('.ALT5').click(function () {
            $('.FUTUREPET').css({textTransform:'none'});
            $('.FUTUREPET').removeClass('ss01 ss02 ss03 ss04 ss06').addClass('ss05');
           
        });
        
        $('.ALT6').click(function () {
            $('.FUTUREPET').css({textTransform:'none'});
            $('.FUTUREPET').removeClass('ss01 ss02 ss03 ss04 ss05').addClass('ss06');
           
        });

        $('.aa').click(function () {
            $('.FUTUREPET').css({textTransform:'lowercase'});
        });
      
        $('.AA').click(function () {
            $('.FUTUREPET').css({textTransform:'uppercase'});
        });
        

        // MIDNAV FUNCTIONS
        $('.web-btn').click(function () {
            $('.web-btn').css({backgroundColor:'#DEDEDE'});
            $('.world-btn').css({backgroundColor:'#DFACFF'});
        });

        $('.world-btn').click(function () {
            $('.world-btn').css({backgroundColor:'#DEDEDE'});
            $('.web-btn').css({backgroundColor:'#DFACFF'});
        });

        $('.web-btn').hover(function () {
            $('.web-btn').css({backgroundColor:'#DEDEDE'});
            $('.world-btn').css({backgroundColor:'#DFACFF'});
        });

        $('.world-btn').hover(function () {
            $('.world-btn').css({backgroundColor:'#DEDEDE'});
            $('.web-btn').css({backgroundColor:'#DFACFF'});
        });



//topnav functions

    $("#trybtn").click(function()
   {
     var elmnt = document.getElementById("typescale");
     elmnt.scrollIntoView();
   });

   $("#stylesbtn").click(function()
   {
     var elmnt = document.getElementById("COL1");
     elmnt.scrollIntoView();
   });

   $("#shopbtn").click(function()
   {
     var elmnt = document.getElementById("shopdiv");
     elmnt.scrollIntoView();
   });


          $("#infobtn").click(function()
 {
   var elmnt = document.getElementById("story");
   elmnt.scrollIntoView();
 });

 $("#getitbtn").click(function()
 {
   var elmnt = document.getElementById("download");
   elmnt.scrollIntoView();
 });

        //mailchimp
        mailchimpForms = new MailchimpForms();
        mailchimpForms.init();

        //resize handler
        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();

        
        $(window).resize(function() {
            delay(function(){
                ref.resize();
            }, 250);
        });
        ref.resize();

        ref.onInputfieldChange();

    };


//slider actions
 

    Controller.prototype.onInputfieldChange = function(){
        
        $("#myRange").on('input', function () {
            var v = $("#myRange").val();
            Logger.log(v)
            $('.FUTUREPET').css('font-size', v + 'vw')
           
        });
      



    }

    Controller.prototype.resize = function()
    {
        $faqItems.each(function(){

        });
    }


    

   

   


    
    /*
     *
     * GENERIC HELPERS - GETTER/SETTER FUNCTIONS
     *
     * */

    //this returns the "real" windows width/height as used in media queries (returns Object{ width:x, height:y })
    Controller.prototype.viewport = function()
    {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }

        var screensize = "screen_xxs";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_xs) screensize = "screen_xs";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_sm) screensize = "screen_sm";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_md) screensize = "screen_md";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_lg) screensize = "screen_lg";
        if(e[ a+'Width' ] >= ref.breakpoints.screen_xlg) screensize = "screen_xlg";

        return { width : e[ a+'Width' ] , height : e[ a+'Height' ], screensize : screensize };
    };

    window.Controller = Controller;

}(window));

